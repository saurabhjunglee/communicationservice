package com.jungleegames.communicationservice.channel.impl;

import org.springframework.stereotype.Component;

import com.jungleegames.communicationservice.channel.CommunicationChannel;
import com.jungleegames.communicationservice.model.Params;
import com.jungleegames.communicationservice.model.Template;

@Component
public class DefaultEmailChannel implements CommunicationChannel {
	
	@Override
	public void send(String eventType, String channel, Params params, Template template) {
		//This is where the code to sent email goes	
		System.out.println("Sending email ... " + template.getTemplateStr() + ", " + params);
	}

	@Override
	public String getType() {
		return "EMAIL";
	}

}
