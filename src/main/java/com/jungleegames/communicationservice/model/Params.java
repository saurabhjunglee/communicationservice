package com.jungleegames.communicationservice.model;

import java.util.Map;
import lombok.Data;

@Data
public class Params {
	private String toAddress;
	private String fromAddress;
	private MessageType messageType;
	private Map<String, String> paramMap;
}
