package com.jungleegames.communicationservice.model;

import java.util.List;

import lombok.Data;

@Data
public class JobRequest {
	private String type;
	private String channel;
	private String templateId;
	private String client;
	
	private List<Task> tasks;
}
