package com.jungleegames.communicationservice.data.repository;

import java.util.UUID;

import org.springframework.data.cassandra.repository.CassandraRepository;

import com.jungleegames.communicationservice.model.Job;

public interface JobRepository extends CassandraRepository<Job, UUID> {

}
