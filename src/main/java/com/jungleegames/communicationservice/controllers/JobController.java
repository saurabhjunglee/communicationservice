package com.jungleegames.communicationservice.controllers;

import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jungleegames.communicationservice.data.repository.JobRepository;
import com.jungleegames.communicationservice.kafka.JobPublisher;
import com.jungleegames.communicationservice.model.Job;
import com.jungleegames.communicationservice.model.JobRequest;


import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/v1/jobs")
public class JobController {

	@Autowired
	private JobRepository jobRepository;
	
	@Autowired
	JobPublisher jobPublisher;
	
	@GetMapping("/{id}")
	public ResponseEntity<Job> getJobById(@PathVariable("id") UUID id) {
		Optional<Job> jobData = jobRepository.findById(id);

		if (jobData.isPresent()) {
			return new ResponseEntity<>(jobData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping
	public ResponseEntity<String> createJob( @RequestBody JobRequest jobReq) {
		
		ListenableFuture<SendResult<String, JobRequest>> jobQueue = jobPublisher.queue(jobReq);
		try {
			SendResult<String, JobRequest> result = jobQueue.get();
			if (result.getRecordMetadata().hasOffset()) {
				return new ResponseEntity<>("Queued", HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>("Failed",HttpStatus.INTERNAL_SERVER_ERROR);
			} 
		} catch (InterruptedException | ExecutionException ex) {
			ex.printStackTrace();
			return new ResponseEntity<>("Failed",HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
