package com.jungleegames.communicationservice.controllers;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jungleegames.communicationservice.data.repository.TaskRepository;
import com.jungleegames.communicationservice.model.Task;
import com.jungleegames.communicationservice.model.TaskStatus;

@RestController
@RequestMapping("/v1/tasks")
public class TaskController {
	@Autowired
	TaskRepository taskRepository;
	
	@GetMapping("/{id}")
	public ResponseEntity<Task> getTaskById(@PathVariable("id") UUID id) {
		Optional<Task> taskData = taskRepository.findById(id);

		if (taskData.isPresent()) {
			return new ResponseEntity<>(taskData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping("/status/{status}")
	public ResponseEntity<List<Task>> getTasksByStatus(@PathVariable("status") String status) {
		List<Task> tasks = taskRepository.findByStatus(TaskStatus.valueOf(status));

		if (tasks != null && tasks.size() != 0) {
			return new ResponseEntity<>(tasks, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping("/job/{jobId}")
	public ResponseEntity<List<Task>> getTasksforJob(@PathVariable("jobId") UUID jobId) {
		List<Task> tasks = taskRepository.findByJobId(jobId);

		if (tasks != null && tasks.size() != 0) {
			return new ResponseEntity<>(tasks, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

}
