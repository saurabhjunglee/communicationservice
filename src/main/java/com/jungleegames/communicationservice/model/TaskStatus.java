package com.jungleegames.communicationservice.model;

public enum TaskStatus {
	COMPLETED,
	SCHEDULED,
	FAILED,
	IN_PROGRESS,
	CREATED
}
