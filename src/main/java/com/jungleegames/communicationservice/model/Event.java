package com.jungleegames.communicationservice.model;

import lombok.Data;

@Data
public class Event {
	private String type;
	private String taskId;
	private String jobId;
	private String channel;
	private String templateId;
	private Params params;
}
