package com.jungleegames.communicationservice.model;

public enum MessageType {
	TRANSACTIONAL,
	PROMOTIONAL
}
