package com.jungleegames.communicationservice.template.repository.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.jungleegames.communicationservice.model.Template;
import com.jungleegames.communicationservice.template.repository.TemplateFetcher;

@Component
public class S3TemplateFetcher implements TemplateFetcher {

	@Override
	public Map<String, Template> loadTemplates() {
		Map<String, Template> templateMap = new HashMap<>();
		Map<String, List<String>> smsEventToChannels = new HashMap<>();
		smsEventToChannels.put("RAF", Arrays.asList("SMS"));
		templateMap.put("raf_sms", new Template("raf_sms", smsEventToChannels));
		Map<String, List<String>> emailEventToChannels = new HashMap<>();
		emailEventToChannels.put("RAF", Arrays.asList("EMAIL"));
		templateMap.put("raf_email", new Template("raf_email", emailEventToChannels));
		return templateMap;
	}

}
