package com.jungleegames.communicationservice.channel.impl;

import org.springframework.stereotype.Component;

import com.jungleegames.communicationservice.channel.CommunicationChannel;
import com.jungleegames.communicationservice.model.Params;
import com.jungleegames.communicationservice.model.Template;

@Component
public class SmsCommunicationChannel implements CommunicationChannel {
	
	@Override
	public void send(String eventType, String channel, Params params, Template template) {
		//This is where code to send sms goes.
		System.out.println("Sending message ... " + template.getTemplateStr() + ", " + params);
	}

	@Override
	public String getType() {
		return "SMS";
	}

}
