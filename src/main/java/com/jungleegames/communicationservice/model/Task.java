package com.jungleegames.communicationservice.model;

import java.util.Map;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import lombok.Data;

@Table
@Data
public class Task {
	@PrimaryKey
	private UUID id;
	
	private UUID jobId;
	private TaskStatus status;
	private MessageType messageType;
	private Map<String, String> paramMap;
	private String toAddress;
	private String fromAddress;
	private int retryCount;
	private int nextRetry;
}
