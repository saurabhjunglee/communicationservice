package com.jungleegames.communicationservice.channel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jungleegames.communicationservice.model.Event;
import com.jungleegames.communicationservice.model.Params;
import com.jungleegames.communicationservice.model.Template;
import com.jungleegames.communicationservice.template.TemplateManager;

@Component
public class ChannelManager {
	
	@Autowired
	private List<CommunicationChannel> channels;
	

	@Autowired
	private TemplateManager templateManager;
	
	private Map<String, CommunicationChannel> channelRegistry;
	
	@PostConstruct
	public void init() {
		channelRegistry = new HashMap<>();
		for (CommunicationChannel channel : channels) {
			channelRegistry.put(channel.getType(), channel);
		}
	}
	
	public void handleEvent(Event event) {
		String eventType = event.getType();
		Params params = event.getParams();
		String channel = event.getChannel();
		Template template;
		if (event.getTemplateId().equals("DEFAULT")) {
			template = templateManager.getTemplate(eventType, channel);			
		} else {
			template = templateManager.getTemplateById(event.getTemplateId());
		}
		channelRegistry.get(channel).send(eventType, channel, params, template);
		//This is where the status of the job/task is to be handled.
	}
}
