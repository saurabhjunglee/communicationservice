package com.jungleegames.communicationservice.template.repository;

import java.util.Map;

import com.jungleegames.communicationservice.model.Template;

public interface TemplateFetcher {
	public Map<String, Template> loadTemplates();
}
