package com.jungleegames.communicationservice.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;

import com.jungleegames.communicationservice.model.JobRequest;

@Component
public class JobPublisher {
	
	@Autowired
	private KafkaTemplate<String, JobRequest> kafkaTemplate;

	@Value("${spring.kafka.job.topic}")
	private String topic;
	
	public ListenableFuture<SendResult<String, JobRequest>> queue(JobRequest job) {
		return kafkaTemplate.send(topic, job);	
	}
}
