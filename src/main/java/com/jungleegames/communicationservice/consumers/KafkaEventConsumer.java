package com.jungleegames.communicationservice.consumers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jungleegames.communicationservice.channel.ChannelManager;
import com.jungleegames.communicationservice.model.Event;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;

@Component
@KafkaListener	(id = "EventHandler", topics = "${spring.kafka.event.topic}")
public class KafkaEventConsumer {
	
	@Autowired
	private ChannelManager channelManager;
	
	@KafkaHandler(isDefault = true)	
	void listen(String eventStr) {
		//Todo:: This code needs to be revisited as automatic deserialisation is failing.
		try { 
		  Event event = new ObjectMapper().readValue(eventStr.getBytes(),
		  Event.class);
		  channelManager.handleEvent(event);		  
		} catch(IOException ex) { 
			ex.printStackTrace(); 
		}
		 
	}
}
