package com.jungleegames.communicationservice.model;

import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import lombok.Builder;
import lombok.Data;

@Table
@Data
@Builder
public class Job {
	
	@PrimaryKey
	private UUID id;
	
	private int totalCount;
	private int completed;
	private String type;
	private String channel;
	private String templateId;
	private String client;
}
