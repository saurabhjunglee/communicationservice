package com.jungleegames.communicationservice.template;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jungleegames.communicationservice.model.Template;
import com.jungleegames.communicationservice.template.repository.TemplateFetcher;

@Component
public class TemplateManager {
	
	@Autowired
	private List<TemplateFetcher> templateFetcher;
	
	private Map<String, Template> templateMap;
	
	private Map<String, String> templateForEventAndChannel;

	@PostConstruct
	public void init() {
		templateMap = new HashMap<>();
		templateForEventAndChannel = new HashMap<>();
		for (TemplateFetcher fetcher : templateFetcher) {
			templateMap.putAll(fetcher.loadTemplates());
		}
		
		for (Map.Entry<String, Template> entry : templateMap.entrySet()) {
			String templateId = entry.getKey();
			Template template = entry.getValue();
			for (Map.Entry<String, List<String>> eventChannels : template.getEventToChannels().entrySet()) {
				String event = eventChannels.getKey();
				for (String channel : eventChannels.getValue()) {
					String key = event + ":" + channel;
					templateForEventAndChannel.put(key, templateId);
				}
			}
		}
	}
	
	public Template getTemplate(String eventType, String channel) {
		String key = eventType + ":" + channel;
		return templateMap.get(templateForEventAndChannel.get(key));
	}
	
	public Template getTemplateById(String templateId) {
		return templateMap.get(templateId);
	}
}
