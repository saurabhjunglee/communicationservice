package com.jungleegames.communicationservice.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import com.jungleegames.communicationservice.model.Event;
import com.jungleegames.communicationservice.model.Job;
import com.jungleegames.communicationservice.model.Params;
import com.jungleegames.communicationservice.model.Task;

@Component
public class EventPublisher {
	
	@Autowired
	private KafkaTemplate<String, Event> kafkaTemplate;
	
	@Value("${spring.kafka.event.topic}")
	private String topic;
	
	public void sendMessage(Job job, Task task) {

		kafkaTemplate.send(topic, task.getId().toString(), createEvent(job, task));	
	}
	
	private Params createParams(Task task) {
		Params params = new Params();
		params.setFromAddress(task.getFromAddress());
		params.setToAddress(task.getToAddress());
		params.setMessageType(task.getMessageType());
		params.setParamMap(task.getParamMap());
		return params;
	}
	
	private Event createEvent(Job job, Task task) {
		Event event = new Event();
		event.setChannel(job.getChannel());
		event.setJobId(job.getId().toString());
		event.setTemplateId(job.getTemplateId());
		event.setType(job.getType());
		event.setParams(createParams(task));
		event.setTaskId(task.getId().toString());
		return event;
	}
}
