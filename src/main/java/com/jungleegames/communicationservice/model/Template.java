package com.jungleegames.communicationservice.model;

import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class Template {
	private String templateStr;
	private Map<String, List<String>> eventToChannels;
	
	public Template(final String atemplateStr, Map<String, List<String>> aEventToChannels) {
		templateStr = atemplateStr;
		eventToChannels = aEventToChannels;
	}
}
