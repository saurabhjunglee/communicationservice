package com.jungleegames.communicationservice.channel;

import com.jungleegames.communicationservice.model.Params;
import com.jungleegames.communicationservice.model.Template;

public interface CommunicationChannel {
	public void send(String eventType, String channel, Params params, Template template);
	
	public String getType();
}
