package com.jungleegames.communicationservice.swagger;

import java.util.function.Predicate;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket communicationservriceApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("communicationservice")
                .apiInfo(apiInfo())
                .select()
                .paths(communicationServicePaths())
                .build();
    }

    private Predicate<String> communicationServicePaths() {
        return PathSelectors.regex("/v1/jobs.*")
        		.or(PathSelectors.regex("/v1/tasks.*"))
        		.or(PathSelectors.regex("/v1/templates.*"));
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("JAPI Communication Service")
                .description("A service to send communication to users")
                .termsOfServiceUrl("http://www.jungleegames.com")
                .contact(new Contact("Saurabh Sisodia", "saurabh@jungleegames.com", "http://www.jungleegames.com"))
                .license("Apache License Version 2.0")
                .licenseUrl("https://github.com/springfox/springfox/blob/master/LICENSE")
                .version("2.0")
                .build();
    }

}
