package com.jungleegames.communicationservice.consumers;

import java.io.IOException;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.datastax.oss.driver.api.core.uuid.Uuids;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jungleegames.communicationservice.data.repository.JobRepository;
import com.jungleegames.communicationservice.data.repository.TaskRepository;
import com.jungleegames.communicationservice.kafka.EventPublisher;
import com.jungleegames.communicationservice.model.Job;
import com.jungleegames.communicationservice.model.JobRequest;
import com.jungleegames.communicationservice.model.Task;
import com.jungleegames.communicationservice.model.TaskStatus;

@Component
@KafkaListener	(id = "JobHandler", topics = "${spring.kafka.job.topic}")
public class KafkaJobHandler {
	@Autowired
	private JobRepository jobRepository;
	
	@Autowired
	TaskRepository taskRepository;
	
	@Autowired
	EventPublisher eventPublisher;
	
	@KafkaHandler(isDefault = true)	
	void listen(String jobStr) {
		//Todo:: This code needs to be revisited as automatic deserialisation is failing.
		try { 
		  JobRequest jobReq = new ObjectMapper().readValue(jobStr.getBytes(), JobRequest.class);
		  try {
				UUID jobId = Uuids.random();
				Job job = Job.builder()
						.channel(jobReq.getChannel())
						.client(jobReq.getClient())
						.totalCount(jobReq.getTasks().size())
						.completed(0)
						.id(jobId)
						.templateId(jobReq.getTemplateId())
						.type(jobReq.getType())
						.build();
				Job _job = jobRepository.save(job);
				for (Task task : jobReq.getTasks()) {
					task.setJobId(jobId);
					task.setId(Uuids.random());
					task.setStatus(TaskStatus.CREATED);
					Task _task = taskRepository.save(task);
					eventPublisher.sendMessage(_job, _task);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}	
		} catch(IOException ex) { 
			ex.printStackTrace(); 
		}
		 
	}
}
