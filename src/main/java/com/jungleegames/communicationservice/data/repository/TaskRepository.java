package com.jungleegames.communicationservice.data.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;

import com.jungleegames.communicationservice.model.Task;
import com.jungleegames.communicationservice.model.TaskStatus;

public interface TaskRepository extends CassandraRepository<Task, UUID> {
	@AllowFiltering
	List<Task> findByStatus(TaskStatus status);
	
	@AllowFiltering
	List<Task> findByJobId(UUID jobId);
}
