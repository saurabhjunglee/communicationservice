package com.jungleegames.communicationservice.kafka;

import java.util.HashMap;

import java.util.Map;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import com.jungleegames.communicationservice.model.Event;
import com.jungleegames.communicationservice.model.JobRequest;

/**
 * KafkaProducerConfig
 */
@Configuration
class KafkaProducerConfig {

	@Value("${spring.kafka.bootstrap-servers}")
	private String bootstrapServers;

	@Bean
	public ProducerFactory<String, Event> eventProducerFactory() {
		Map<String, Object> configProps = new HashMap<>();
		configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);

		return new DefaultKafkaProducerFactory<>(configProps, new StringSerializer(), new JsonSerializer<Event>());
	}

	@Bean
	public KafkaTemplate<String, Event> eventKafkaTemplate() {
		return new KafkaTemplate<>(eventProducerFactory());
	}
	
	@Bean
	public ProducerFactory<String, JobRequest> jobproducerFactory() {
		Map<String, Object> configProps = new HashMap<>();
		configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);

		return new DefaultKafkaProducerFactory<>(configProps, new StringSerializer(), new JsonSerializer<JobRequest>());
	}
	
	@Bean
	public KafkaTemplate<String, JobRequest> kafkaTemplate() {
		return new KafkaTemplate<>(jobproducerFactory());
	}
}
